//
//  GridCustomCollectionViewController.swift
//  HeroLibrarySample
//
//  Created by Hiem Seyha on 3/29/17.
//  Copyright © 2017 seyha. All rights reserved.
//

import UIKit
import Hero

class GridCustomCollectionViewCell: UICollectionViewCell {
  
  
  @IBOutlet weak var myImageView: UIImageView!
  
}

class GridCustomCollectionViewController: UIViewController {

  @IBOutlet weak var myCollectionView: UICollectionView!
  
  let foods = [#imageLiteral(resourceName: "Unsplash7"),#imageLiteral(resourceName: "Unsplash7"),#imageLiteral(resourceName: "Unsplash7"),#imageLiteral(resourceName: "Unsplash7"),#imageLiteral(resourceName: "Unsplash7"),#imageLiteral(resourceName: "Unsplash7"),#imageLiteral(resourceName: "Unsplash7"),#imageLiteral(resourceName: "Unsplash7"),#imageLiteral(resourceName: "Unsplash7"),#imageLiteral(resourceName: "Unsplash7"),#imageLiteral(resourceName: "Unsplash7"),#imageLiteral(resourceName: "Unsplash7"),#imageLiteral(resourceName: "Unsplash7"),#imageLiteral(resourceName: "Unsplash7"),#imageLiteral(resourceName: "Unsplash7"),#imageLiteral(resourceName: "Unsplash7"),#imageLiteral(resourceName: "Unsplash7"),#imageLiteral(resourceName: "Unsplash7"),#imageLiteral(resourceName: "Unsplash7"),#imageLiteral(resourceName: "Unsplash7")]
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    myCollectionView.delegate = self
    myCollectionView.dataSource = self
    isHeroEnabled = true

  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
    myCollectionView.heroModifiers = [.cascade]
  }

}

extension GridCustomCollectionViewController:UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
      let width = myCollectionView.frame.width / 5 - 1
      let height = myCollectionView.frame.height / 5 - 1
    return CGSize(width: width, height: height)
    
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return foods.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    let cell = myCollectionView.dequeueReusableCell(withReuseIdentifier: "item", for: indexPath) as! GridCustomCollectionViewCell
//    cell.myImageView.image = foods[indexPath.row]
    cell.backgroundColor = UIColor.red
    cell.heroModifiers = [.fade,.scale(0.5)]

    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 2
  }
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 1
  }
  
}
